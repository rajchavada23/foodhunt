import React from 'react';

//import screens
import LoginScreen from './src/screen/login/LoginScreen';
import SignupScreen from './src/screen/signup/SignupScreen';
import VerifyNumberScreen from './src/screen/verifyNumber/VerifyNumberScreen';
// import HomeScreen from './src/screen/HomeScreen/HomeScreen';
import AddressScreen from './src/screen/AddressScreen/AddressScreen';
import MyTabs from './src/components/MyTabs/MyTabs';
import ManageAddressScreen from './src/screen/ManageAddressScreen/ManageAddressScreen';
import FoodDetail from './src/screen/FoodDetail';
import AddNewAddress from './src/screen/AddNewAddress/AddNewAddress';

//Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

//ui-kitten
import * as eva from '@eva-design/eva';
import {ApplicationProvider} from '@ui-kitten/components';

const Stack = createStackNavigator();

const App = () => {
  return (
    <ApplicationProvider {...eva} theme={eva.light}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="login"
            component={LoginScreen}
            options={({route}) => ({
              headerShown: false,
            })}
          />
          <Stack.Screen
            name="signup"
            component={SignupScreen}
            options={({route}) => ({
              headerShown: false,
            })}
          />
          <Stack.Screen
            name="verifynumber"
            component={VerifyNumberScreen}
            options={({route}) => ({
              headerShown: false,
            })}
          />
          {/* <Stack.Screen
            name="homescreen"
            component={HomeScreen}
            options={({route}) => ({
              headerShown: false,
            })}
          /> */}
          <Stack.Screen
            name="addressScreen"
            component={AddressScreen}
            options={({route}) => ({
              headerShown: false,
            })}
          />
          <Stack.Screen
            name="bottomBar"
            component={MyTabs}
            options={({route}) => ({
              headerShown: false,
            })}
          />
          <Stack.Screen
            name="manageAddressScreen"
            component={ManageAddressScreen}
            options={({route}) => ({
              headerShown: false,
            })}
          />
          <Stack.Screen
            name="FoodDetail"
            component={FoodDetail}
            options={({route}) => ({
              headerShown: false,
            })}
          />
          <Stack.Screen
            name="AddNewAddress"
            component={AddNewAddress}
            options={({route}) => ({
              headerShown: false,
            })}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </ApplicationProvider>
  );
};

export default App;

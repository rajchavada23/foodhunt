import React from 'react';

//import screens
import MyTabs from './src/components/MyTabs/MyTabs';

//Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

//ui-kitten
// import * as eva from '@eva-design/eva';
import {ApplicationProvider} from '@ui-kitten/components';

const Stack = createStackNavigator();

const App = () => {
  return (
    <ApplicationProvider>
      <NavigationContainer>
        <Stack.Navigator>
          <MyTabs />
        </Stack.Navigator>
      </NavigationContainer>
    </ApplicationProvider>
  );
};

export default App;

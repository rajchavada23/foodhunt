import React from 'react';

//import screens
import HomeScreen from '../../screen/HomeScreen/HomeScreen';
import OfferScreen from '../../screen/offerScreen/OfferScreen';
import SearchScreen from '../../screen/searchScreen/SearchScreen';
import CartScreen from '../../screen/CartScreen/CartScreen';
import MoreScreen from '../../screen/moreScreen/MoreScreen';

//Icon Path
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

//Navigation
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const ACTIVE_TAB_COLOR = '#284AAD';
const INACTIVE_TAB_COLOR = '#1C1A2E';

const Tab = createBottomTabNavigator();

const MyTabs = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        keyboardHidesTabBar: true,
        activeTintColor: '#284AAD',
        inactiveTintColor: '#1C1A2E',
        style: {
          height: 70,
          paddingTop: 12,
          paddingBottom: 6,
        },
        labelStyle: {
          fontSize: 17,
          fontFamily: 'Nunito-ExtraBold',
          marginVertical: 5,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({focused}) => (
            <Entypo
              name="home"
              size={30}
              focused={focused}
              color={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Offer"
        component={OfferScreen}
        options={{
          tabBarLabel: 'Offers',
          tabBarIcon: ({focused}) => (
            <Ionicons
              name="ios-flash"
              size={30}
              focused={focused}
              color={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Search"
        component={SearchScreen}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({focused}) => (
            <FontAwesome
              name="search"
              size={30}
              focused={focused}
              color={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Cart"
        component={CartScreen}
        options={{
          tabBarLabel: 'Cart',
          tabBarIcon: ({focused}) => (
            <Ionicons
              name="md-cart"
              size={30}
              focused={focused}
              color={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR}
            />
          ),
        }}
      />
      <Tab.Screen
        name="More"
        component={MoreScreen}
        options={{
          tabBarLabel: 'More',
          tabBarIcon: ({focused}) => (
            <Feather
              name="more-horizontal"
              size={30}
              focused={focused}
              color={focused ? ACTIVE_TAB_COLOR : INACTIVE_TAB_COLOR}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default MyTabs;

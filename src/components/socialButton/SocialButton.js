import React from 'react';
import {TouchableOpacity, View, StyleSheet, Dimensions} from 'react-native';
import Text from '../Text';
import {COLORS, FONT_MEASURMENT} from '../../helper/Constants';

import {useNavigation} from '@react-navigation/native';

const width = Dimensions.get('window').width;

const SocialButton = props => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('signup');
      }}
      style={[styles.faceButtonStyle, {backgroundColor: props.color}]}>
      <View style={styles.socialLinkView}>
        {props.icon}
        <Text style={styles.socialText}>{props.title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default SocialButton;

const styles = StyleSheet.create({
  socialLinkView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  faceButtonStyle: {
    marginVertical: 12,
    paddingVertical: 10,
    borderRadius: 5,
    width: width - 120,
  },
  socialText: {
    color: COLORS.APP_PRIMARY,
    fontSize: FONT_MEASURMENT.medium,
  },
});

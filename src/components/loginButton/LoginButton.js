import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {COLORS, FONT_MEASURMENT} from '../../helper/Constants';

// Relative path
import Text from '../Text';

const LoginButton = props => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.buttonContainer}
      onPress={() => {
        navigation.navigate('bottomBar');
      }}>
      <Text style={styles.buttontext}>{props.title}</Text>
    </TouchableOpacity>
  );
};

export default LoginButton;

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: COLORS.ORANGE,
    alignItems: 'center',
    paddingVertical: 15,
    marginVertical: 15,
  },
  buttontext: {
    color: COLORS.APP_PRIMARY,
    fontSize: FONT_MEASURMENT.large,
  },
});

import {StyleSheet} from 'react-native';
import {
  HEIGHT,
  COLORS,
  WIDTH,
  FONTS,
  FONT_MEASURMENT,
} from '../../helper/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.LIGHT_GRAY,
  },
  mainView: {
    flex: 1,
  },
  headerView: {
    flexDirection: 'row',
    marginBottom: 15,
    paddingVertical: 8,
    backgroundColor: COLORS.WHITE,
    paddingHorizontal: 16,
    alignItems: 'center',
  },
  mainHeader: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  mainHeaderText: {
    marginLeft: 10,
    flex: 1,
  },
  headerHomeText: {
    flexDirection: 'row',
  },
  headertext1: {
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.EXTRA_BOLD,
    color: COLORS.BLUE,
  },
  headertext2: {
    color: COLORS.WORKICON_COLOR,
    fontSize: FONT_MEASURMENT.medium,
  },
  downArrowIcon: {
    color: COLORS.BLUE,
    paddingLeft: 8,
  },
  smileTextView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  smileText: {
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.BOLD,
    paddingRight: 5,
  },
  bodyContainer: {
    flex: 1,
  },
  bodyMainView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageView: {
    borderRadius: 5,
  },
  image: {
    width: WIDTH / 2 - 20,
    height: WIDTH / 2 - 20,
    marginHorizontal: 5,
    marginBottom: 10,
    borderRadius: 5,
    backgroundColor: COLORS.BLACK,
    opacity: 0.7,
  },
  imgOffer: {
    fontSize: FONT_MEASURMENT.small,
    position: 'absolute',
    backgroundColor: COLORS.APP_PRIMARY,
    margin: 10,
    paddingVertical: 1,
    paddingHorizontal: 3,
  },
  imgTextView: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 10,
  },
  bigtext: {
    fontSize: FONT_MEASURMENT.medium,
    lineHeight: 18,
    fontFamily: FONTS.EXTRA_BOLD,
    color: COLORS.APP_PRIMARY,
  },
  smalltext: {
    fontSize: 10,
    fontFamily: FONTS.BOLD,
    color: COLORS.APP_PRIMARY,
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
    marginHorizontal: 16,
  },
  nearText: {
    flexDirection: 'row',
  },
  text1: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
  },
  text2: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
    color: COLORS.ORANGE,
  },
  text3: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
    paddingHorizontal: 10,
  },
  imgViewContainer: {
    flexDirection: 'row',
    padding: 8,
    borderRadius: 10,
    marginVertical: 5,
    backgroundColor: COLORS.WHITE,
  },
  nearImage: {
    width: 110,
    height: 110,
    marginRight: 18,
    borderRadius: 5,
  },
  border1: {
    width: 240,
    borderTopWidth: 1,
    borderTopColor: '#CED0D5',
    marginVertical: 5,
  },
  reviewTextContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  //restaurant cards
  title: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
  },
  state: {
    fontSize: FONT_MEASURMENT.medium,
    fontFamily: FONTS.REGULAR,
  },
  city: {
    fontFamily: FONTS.REGULAR,
  },
});

import React from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Image,
  ImageBackground,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

// Relative path
import {styles} from './HomeScreenStyle';
import Text from '../../components/Text';
import {COLORS} from '../../helper/Constants';

// Helper
import {dataList, dataFlatList} from '../../helper/jsonData';

// Icon path
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const HomeScreen = () => {
  const navigation = useNavigation();

  const onFoodClicked = (item, index) => {
    navigation.navigate('FoodDetail', {foodData: item});
  };

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{flexGrow: 1}}>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.mainView}>
            <View style={styles.headerView}>
              <TouchableOpacity>
                <Feather name="menu" size={30} style={styles.menuIcon} />
              </TouchableOpacity>
              <View style={styles.mainHeader}>
                <View style={styles.mainHeaderText}>
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('addressScreen');
                    }}>
                    <View style={styles.headerHomeText}>
                      <Text style={styles.headertext1}>HOME</Text>
                      <FontAwesome
                        name="angle-down"
                        size={30}
                        style={styles.downArrowIcon}
                      />
                    </View>
                    <Text numberOfLines={1} style={styles.headertext2}>
                      31 A,RK Flats, near Vasushisti, blaldqasdlasdlasdla
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.smileTextView}>
                  <FontAwesome5
                    name="smile"
                    size={30}
                    style={styles.smileIcon}
                  />
                </View>
              </View>
            </View>
            <FlatList
              data={dataList}
              contentContainerStyle={{
                alignItems: 'center',
              }}
              keyExtractor={(item, index) => `${index}`}
              numColumns={2}
              renderItem={({item, index}) => {
                return (
                  <TouchableOpacity>
                    <Image source={item.image} style={styles.image} />
                    <View
                      style={{
                        position: 'absolute',
                        height: '100%',
                        padding: 5,
                      }}>
                      <Text style={styles.imgOffer}>OFFER</Text>
                      <View style={styles.imgTextView}>
                        <Text style={styles.bigtext}>{item.bigtitle}</Text>
                        <Text numberOfLines={1} style={styles.smalltext}>
                          {item.smalltitle}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
            <View style={styles.textContainer}>
              <TouchableOpacity style={styles.nearText}>
                <Text style={styles.text1}>Top 47 Restaurants </Text>
                <Text style={styles.text2}>near you</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.nearText}>
                <Fontisto name="equalizer" size={20} />
                <Text style={styles.text3}>Filter</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              data={dataFlatList}
              keyExtractor={(item, index) => `${index}`}
              numColumns={1}
              contentContainerStyle={{paddingHorizontal: 10}}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => {
                return (
                  <TouchableOpacity
                    style={styles.imgViewContainer}
                    onPress={() => onFoodClicked(item, index)}>
                    <Image source={item.image} style={styles.nearImage} />
                    <View style={{flex: 1}}>
                      <View style={{flex: 1}}>
                        <Text style={styles.title} numberOfLines={1}>
                          {item.foodname}
                        </Text>
                        <Text style={styles.state}>{item.states}</Text>
                        <Text style={styles.city}>{item.city}</Text>
                      </View>
                      <View style={styles.reviewTextContainer}>
                        <View style={styles.nearText}>
                          <AntDesign name="star" size={15} />
                          <Text>4.6</Text>
                        </View>
                        <Text>Deliver in 20 min.</Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              }}
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default HomeScreen;

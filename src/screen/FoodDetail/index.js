import React, {useEffect} from 'react';
import {View, Text} from 'react-native';

const Index = props => {
  useEffect(() => {
    console.log(props.route.params.foodData);
    return () => {};
  }, [props]);

  return renderMainView();
};

const renderMainView = () => {
  return (
    <View>
      <Text>hello</Text>
    </View>
  );
};

export default Index;

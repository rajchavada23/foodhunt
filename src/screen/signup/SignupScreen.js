import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {CheckBox} from '@ui-kitten/components';

// Relative path
import {styles} from './SignupStyles';
import Text from '../../components/Text';
import {COLORS} from '../../helper/Constants';

// Icon path
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const SignupScreen = () => {
  const navigation = useNavigation();
  const [checked, setChecked] = React.useState(false);
  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <AntDesign name="arrowleft" size={30} style={styles.arrowIcon} />
          </TouchableOpacity>
          <View style={styles.mainHeader}>
            <View style={styles.mainHeaderText}>
              <Text style={styles.headertext1}>CREATE ACCOUNT</Text>
              <Text style={styles.headertext2}>
                Create an account with the new phone number
              </Text>
            </View>
            <Image
              style={styles.coffeeImg}
              source={require('./../../../assets/images/food-and-restaurant.png')}
            />
          </View>
        </View>
        <View style={[styles.mainView, {backgroundColor: COLORS.APP_PRIMARY}]}>
          <View style={styles.inputcontainer}>
            <View style={styles.inputStyle}>
              <TextInput
                style={styles.textInput}
                placeholder="10 Digit mobile number"
                keyboardType="phone-pad"
              />
            </View>
            <View style={styles.inputStyle}>
              <TextInput
                style={styles.textInput}
                placeholder="Enter Your Full Name"
                keyboardType="default"
              />
            </View>
            <View style={styles.inputStyle}>
              <TextInput
                style={styles.textInput}
                placeholder="Enter Email ID"
                keyboardType="email-address"
              />
            </View>
            <View style={styles.passinputContainer}>
              <TextInput
                style={styles.textInput}
                placeholder="Enter Password"
                keyboardType="default"
                secureTextEntry={true}
              />
              <MaterialIcons
                name="remove-red-eye"
                size={30}
                style={styles.eyeIcon}
              />
            </View>
            <CheckBox
              style={styles.checkBox}
              checked={checked}
              onChange={nextChecked => setChecked(nextChecked)}>
              <Text style={styles.checkBoxText}>I have referral code</Text>
            </CheckBox>
            <TouchableOpacity
              style={styles.createButton}
              onPress={() => {
                navigation.navigate('verifynumber');
              }}>
              <Text style={styles.buttontext}>CREATE ACCOUNT</Text>
            </TouchableOpacity>
            <Text style={styles.termstext}>
              By creating account, you accept terms and conditions
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default SignupScreen;

import {StyleSheet} from 'react-native';
import {
  HEIGHT,
  COLORS,
  WIDTH,
  FONTS,
  FONT_MEASURMENT,
} from '../../helper/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    backgroundColor: COLORS.HEADER_BG_COLOR,
  },
  arrowIcon: {
    margin: 20,
  },
  mainHeader: {
    marginHorizontal: 20,
    flexDirection: 'row',
  },
  mainHeaderText: {
    flex: 1,
    marginBottom: 45,
  },
  headertext1: {
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.BOLD,
    paddingBottom: 5,
  },
  headertext2: {
    color: '#00000070',
    fontSize: FONT_MEASURMENT.small,
  },
  coffeeImg: {
    width: 52,
    height: 52,
    resizeMode: 'contain',
    marginHorizontal: 25,
  },
  mainView: {
    flex: 1,
  },
  inputcontainer: {
    marginHorizontal: 20,
    marginTop: 18,
  },
  inputStyle: {
    borderBottomWidth: 1,
    borderColor: '#00000030',
    marginVertical: 8,
    paddingLeft: 3,
  },
  passinputContainer: {
    borderBottomWidth: 1,
    borderColor: '#00000030',
    marginVertical: 8,
    flexDirection: 'row',
    paddingLeft: 3,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textInput: {
    fontSize: FONT_MEASURMENT.medium,
    fontFamily: FONTS.BOLD,
  },
  eyeIcon: {
    color: '#00000070',
  },
  checkBox: {
    marginVertical: 20,
  },
  checkBoxText: {
    fontSize: FONT_MEASURMENT.medium,
    color: '#858587',
    fontFamily: FONTS.BOLD,
  },
  createButton: {
    backgroundColor: '#BDC0C6',
    alignItems: 'center',
    paddingVertical: 15,
    marginVertical: 15,
  },
  buttontext: {
    color: COLORS.APP_PRIMARY,
    fontSize: FONT_MEASURMENT.large,
  },
  termstext: {
    fontFamily: FONTS.BOLD,
  },
});

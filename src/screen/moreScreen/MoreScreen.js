import React from 'react';
import {View, TouchableOpacity, ScrollView, FlatList} from 'react-native';
import Dash from 'react-native-dash';

//Navigation
import {useNavigation} from '@react-navigation/native';

// Relative path
import {styles} from './MoreScreenStyle';
import Text from '../../components/Text';
import {COLORS} from '../../helper/Constants';

//Icon Path
import Entypo from 'react-native-vector-icons/Entypo';

const ADDRESS_CATEGORIES = [
  {img: 'M', title: 'Manage Address'},
  {img: '$', title: 'Payment'},
  {img: 'F', title: 'Favourites'},
  {img: 'R', title: 'referrals'},
  {img: 'O', title: 'Offers'},
];

const MoreScreen = () => {
  const navigation = useNavigation();
  const _onAddressCategoryPressed = (item, index) => {
    switch (index) {
      case 0:
        navigation.navigate('manageAddressScreen');
        break;
      case 1:
        navigation.navigate('manageAddressScreen');
        break;
      case 2:
        navigation.navigate('manageAddressScreen');
        break;
      case 3:
        navigation.navigate('manageAddressScreen');
        break;
      case 4:
        navigation.navigate('manageAddressScreen');
        break;
      default:
        break;
    }
  };
  return (
    <ScrollView
      contentContainerStyle={{flexGrow: 1}}
      showsVerticalScrollIndicator={false}>
      <View style={[styles.container, {backgroundColor: COLORS.APP_PRIMARY}]}>
        <View style={styles.mainContainer}>
          <View style={styles.containerView}>
            <View style={styles.userInfoView}>
              <View style={styles.userInfoEdit}>
                <Text style={styles.userNameText}>JOHN WOOSH</Text>
                <TouchableOpacity>
                  <Text style={styles.editText}>EDIT</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.userInfo}>
                <Text style={styles.userInfoText}>318738713</Text>
                <Text style={styles.verticalLine}>|</Text>
                <Text style={styles.userInfoText}>john@gmail.com</Text>
              </View>
            </View>
            <View style={styles.border1} />
            <View style={styles.userInfoView}>
              <Text style={styles.userNameText}>MY ACCOUNTS</Text>
              <View style={styles.accountsInfo}>
                <Text style={styles.accountsText}>
                  Addresses,Payments,Favourites,Redderals & Offers
                </Text>
                <TouchableOpacity>
                  <Entypo name="chevron-up" size={30} />
                </TouchableOpacity>
              </View>
            </View>
            <Dash dashColor="#90959F" />
            <View style={styles.dropdownView}>
              <FlatList
                data={ADDRESS_CATEGORIES}
                keyExtractor={(item, index) => `${index}`}
                renderItem={({item, index}) => {
                  return (
                    <TouchableOpacity
                      onPress={() => _onAddressCategoryPressed(item, index)}
                      style={styles.addressView}>
                      <View style={styles.mSymbolView}>
                        <Text style={styles.mSymbolText}>{item.img}</Text>
                      </View>
                      <View style={styles.manAddView}>
                        <Text style={styles.manAddText}>{item.title}</Text>
                        <Entypo name="chevron-small-right" size={30} />
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            </View>
            <View style={styles.border1} />
            <View style={styles.userInfoView}>
              <Text style={styles.userNameText}>HELP</Text>
              <View style={styles.accountsInfo}>
                <Text style={styles.accountsText}>
                  Help center related your orders and queries
                </Text>
                <TouchableOpacity>
                  <Entypo name="chevron-small-right" size={30} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.border1} />
            <View style={styles.userInfoView}>
              <View style={styles.accountsInfo}>
                <Text style={styles.userNameText}>RECENT ORDERS</Text>
                <TouchableOpacity>
                  <Entypo name="chevron-small-down" size={30} />
                </TouchableOpacity>
              </View>
              <Text style={styles.accountsText}>View all recent orders</Text>
            </View>
          </View>
          <View style={styles.horizontalLine} />
          <TouchableOpacity style={styles.logoutButton}>
            <Text style={styles.logoutText}>LOGOUT</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default MoreScreen;

import {StyleSheet} from 'react-native';
import {
  HEIGHT,
  COLORS,
  WIDTH,
  FONTS,
  FONT_MEASURMENT,
} from '../../helper/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerView: {
    width: WIDTH - 50,
  },
  userInfoView: {
    marginVertical: 20,
  },
  userInfoEdit: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  userNameText: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.EXTRA_BOLD,
  },
  editText: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
    color: COLORS.ORANGE,
  },
  userInfo: {
    flexDirection: 'row',
  },
  userInfoText: {
    fontSize: FONT_MEASURMENT.medium,
    fontFamily: FONTS.BOLD,
    color: COLORS.WORKICON_COLOR,
  },
  verticalLine: {
    paddingHorizontal: 10,
  },
  border1: {
    borderWidth: 1,
  },
  accountsInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  accountsText: {
    fontSize: 13,
    color: '#90959F',
    fontFamily: FONTS.BOLD,
  },
  dropdownView: {
    marginVertical: 5,
  },
  addressView: {
    flexDirection: 'row',
    marginVertical: 15,
  },
  mSymbolView: {
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 3,
    paddingHorizontal: 8,
    marginRight: 20,
  },
  mSymbolText: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
  },
  manAddView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  manAddText: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
  },
  rupeeIcon: {
    paddingHorizontal: 2.5,
  },
  fSymbolText: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
    paddingHorizontal: 2.5,
  },
  rSymbolText: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
    paddingHorizontal: 1.4,
  },
  oSymbolText: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.BOLD,
    paddingHorizontal: 0.5,
  },
  horizontalLine: {
    width: WIDTH - 15,
    borderWidth: 10,
    borderColor: '#E4E5E8',
  },
  logoutButton: {
    width: WIDTH - 50,
    marginTop: 20,
    marginBottom: 30,
  },
  logoutText: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.EXTRA_BOLD,
  },
});

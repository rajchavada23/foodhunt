import React from 'react';
import {
  TouchableWithoutFeedback,
  Keyboard,
  View,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';

// Relative path
import {styles} from './SearchScreenStyle';
import Text from '../../components/Text';
import {COLORS} from '../../helper/Constants';

//Icon Path
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const RECENT_SEARCH = [
  {title: 'Brown Burger'},
  {title: 'Pav Bhaji'},
  {title: 'Cafe Mocha'},
];

const SearchScreen = () => {
  const [value, setValue] = React.useState('');

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <View style={[styles.container, {backgroundColor: COLORS.APP_PRIMARY}]}>
        <View style={styles.searchContainer}>
          <View style={styles.searchView}>
            <View style={styles.searchBar}>
              <FontAwesome name="search" size={25} style={styles.searchIcon} />
              <TextInput
                placeholder="Search Restaurant/Cafe"
                style={styles.textInputStyle}
                value={value}
                onChangeText={nextValue => setValue(nextValue)}
              />
            </View>
            <View style={styles.recentSearchView}>
              <Text style={styles.recentSearchText}>RECENT SEARCH</Text>
              <View style={styles.border1} />
              <FlatList
                data={RECENT_SEARCH}
                keyExtractor={(item, index) => `${index}`}
                renderItem={({item, index}) => {
                  return (
                    <View>
                      <TouchableOpacity style={styles.recentSearchBar}>
                        <FontAwesome
                          name="search"
                          size={25}
                          style={styles.resentSearchIcon}
                        />
                        <Text>{item.title}</Text>
                      </TouchableOpacity>
                      <View style={styles.border2} />
                    </View>
                  );
                }}
              />
            </View>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default SearchScreen;

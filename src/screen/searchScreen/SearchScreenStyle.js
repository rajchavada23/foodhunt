import {StyleSheet} from 'react-native';
import {
  HEIGHT,
  COLORS,
  WIDTH,
  FONTS,
  FONT_MEASURMENT,
} from '../../helper/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchView: {
    width: WIDTH - 50,
  },
  searchBar: {
    flexDirection: 'row',
    backgroundColor: '#EFEFEF',
    borderRadius: 5,
    marginVertical: 35,
  },
  searchIcon: {
    paddingVertical: 15,
    paddingHorizontal: 15,
  },
  textInputStyle: {
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.EXTRA_BOLD,
  },
  recentSearchText: {
    fontSize: FONT_MEASURMENT.title,
    fontFamily: FONTS.EXTRA_BOLD,
    paddingBottom: 20,
  },
  border1: {
    borderWidth: 1,
  },
  recentSearchBar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  resentSearchIcon: {
    paddingRight: 15,
    paddingVertical: 15,
  },
  border2: {
    borderWidth: 0.4,
    borderColor: '#C7CACF',
  },
});

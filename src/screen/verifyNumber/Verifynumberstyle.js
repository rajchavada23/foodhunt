import {StyleSheet} from 'react-native';
import {
  HEIGHT,
  COLORS,
  WIDTH,
  FONTS,
  FONT_MEASURMENT,
} from '../../helper/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    backgroundColor: COLORS.HEADER_BG_COLOR,
  },
  arrowIcon: {
    margin: 20,
  },
  mainHeader: {
    marginHorizontal: 20,
    flexDirection: 'row',
  },
  mainHeaderText: {
    flex: 1,
    marginBottom: 45,
  },
  headertext1: {
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.EXTRA_BOLD,
    paddingBottom: 5,
  },
  headertext2: {
    color: '#00000070',
    fontSize: FONT_MEASURMENT.medium,
  },
  verifyImg: {
    height: 65,
    resizeMode: 'contain',
    width: WIDTH - 250,
  },
  mainView: {
    flex: 1,
    backgroundColor: COLORS.APP_PRIMARY,
  },
  inputcontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputStyle: {
    width: WIDTH - 160,
  },
  underlineStyleBase: {
    borderWidth: 0,
    borderBottomWidth: 1,
    fontSize: FONT_MEASURMENT.large,
    color: COLORS.BLACK,
  },
  underlineStyleHighLighted: {
    borderColor: '#03DAC6',
  },
  mainButtonView: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 10,
    marginHorizontal: 20,
  },
  verifyButton: {
    backgroundColor: COLORS.GREEN,
    alignItems: 'center',
    paddingVertical: 15,
    marginVertical: 15,
  },
  buttontext: {
    color: COLORS.APP_PRIMARY,
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.EXTRA_BOLD,
  },
});

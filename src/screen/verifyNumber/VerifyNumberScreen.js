import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import OTPInputView from '@twotalltotems/react-native-otp-input';

// Relative path
import {styles} from './Verifynumberstyle';
import Text from '../../components/Text';

// Icon path
import AntDesign from 'react-native-vector-icons/AntDesign';

const VerifyNumberScreen = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <AntDesign name="arrowleft" size={30} style={styles.arrowIcon} />
        </TouchableOpacity>
        <View style={styles.mainHeader}>
          <View style={styles.mainHeaderText}>
            <Text style={styles.headertext1}>VERIFY NUMBER</Text>
            <Text style={styles.headertext2}>OTP send to 9549924343</Text>
          </View>
          <Image
            style={styles.verifyImg}
            source={require('./../../../assets/images/telephone.png')}
          />
        </View>
      </View>
      <View style={styles.mainView}>
        <View style={styles.inputcontainer}>
          <View style={styles.inputStyle}>
            <OTPInputView
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              pinCount={4}
            />
          </View>
        </View>
        <View style={styles.mainButtonView}>
          <TouchableOpacity style={styles.verifyButton}>
            <Text style={styles.buttontext}>VERIFY</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default VerifyNumberScreen;

import {StyleSheet} from 'react-native';
import {
  HEIGHT,
  COLORS,
  WIDTH,
  FONTS,
  FONT_MEASURMENT,
} from '../../helper/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 15,
  },
  headerView: {
    flexDirection: 'row',
    width: WIDTH - 30,
  },
  arrowIcon: {
    margin: 5,
  },
  mainHeader: {
    marginBottom: 6,
  },
  mainHeaderText: {
    marginHorizontal: 20,
  },
  headertext1: {
    color: COLORS.ORANGE,
    fontSize: FONT_MEASURMENT.small,
    fontFamily: FONTS.BOLD,
  },
  headertext2: {
    fontSize: FONT_MEASURMENT.large,
    color: '#90959F',
    fontFamily: 'Nunito-Bold',
  },
  border: {
    borderTopWidth: 1,
    borderTopColor: '#CED0D5',
  },
  viewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainView: {
    width: WIDTH - 50,
  },
  currnetLocView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 20,
  },
  targetIcon: {
    color: COLORS.BLUE,
  },
  currLoctext: {
    paddingLeft: 20,
  },
  currLoctext1: {
    color: COLORS.BLUE,
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.BOLD,
  },
  currLoctext2: {
    color: COLORS.WORKICON_COLOR,
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.BOLD,
  },
  border1: {
    borderTopWidth: 2,
    borderTopColor: COLORS.BLACK,
  },
  svdTextView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  svdText: {
    width: WIDTH - 170,
    fontSize: FONT_MEASURMENT.medium,
    fontFamily: FONTS.EXTRA_BOLD,
  },
  addressView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 20,
  },
  saveAddtext1: {
    fontSize: FONT_MEASURMENT.large,
    fontFamily: 'Nunito-Bold',
  },
  saveAddtext2: {
    color: COLORS.WORKICON_COLOR,
    fontSize: FONT_MEASURMENT.medium,
    fontFamily: FONTS.REGULAR,
  },
  border2: {
    marginLeft: 60,
    borderTopWidth: 1,
    borderTopColor: '#CED0D5',
  },
  workIcon: {
    color: COLORS.WORKICON_COLOR,
  },
});

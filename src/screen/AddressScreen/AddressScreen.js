import React from 'react';
import {View, TouchableOpacity, FlatList, ScrollView} from 'react-native';

// Navigation
import {useNavigation} from '@react-navigation/native';

// Relative path
import {styles} from './AddressScreenStyle';
import Text from '../../components/Text';
import {COLORS} from '../../helper/Constants';

// Helper
import {RECENT_SEARCHES1, RECENT_SEARCHES2} from '../../helper/jsonData';

// Icon path
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const AddressScreen = () => {
  const navigation = useNavigation();
  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <View style={[styles.container, {backgroundColor: COLORS.APP_PRIMARY}]}>
        <View style={styles.headerContainer}>
          <View style={styles.headerView}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}>
              <AntDesign name="arrowleft" size={30} style={styles.arrowIcon} />
            </TouchableOpacity>
            <View style={styles.mainHeader}>
              <View style={styles.mainHeaderText}>
                <TouchableOpacity>
                  <Text style={styles.headertext1}>SET DELIVERY LOCATION</Text>
                  <Text style={styles.headertext2}>
                    Search for area, street name...
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.border} />
        <View style={styles.viewContainer}>
          <View style={styles.mainView}>
            <View style={styles.currnetLocView}>
              <MaterialCommunityIcons
                name="target"
                size={40}
                style={styles.targetIcon}
              />
              <TouchableOpacity style={styles.currLoctext}>
                <Text style={styles.currLoctext1}>Current Location</Text>
                <Text style={styles.currLoctext2}>
                  Enable location services
                </Text>
              </TouchableOpacity>
            </View>
            <View>
              <View style={styles.border1} />
              <View style={styles.savedAddView}>
                <View style={styles.svdTextView}>
                  <Text style={styles.svdText}>SAVED ADDRESSES</Text>
                </View>
                <FlatList
                  data={RECENT_SEARCHES1}
                  keyExtractor={(item, index) => `${index}`}
                  renderItem={({item, index}) => {
                    return (
                      <View>
                        <View style={styles.addressView}>
                          {item.type === 'office' ? (
                            <MaterialIcons
                              name="work"
                              size={40}
                              style={styles.workIcon}
                            />
                          ) : (
                            <Feather
                              name="map-pin"
                              size={40}
                              style={styles.workIcon}
                            />
                          )}

                          <TouchableOpacity style={styles.currLoctext}>
                            <Text style={styles.saveAddtext1}>
                              {item.title}
                            </Text>
                            <Text style={styles.saveAddtext2}>
                              {item.address}
                            </Text>
                          </TouchableOpacity>
                        </View>
                        <View style={styles.border2} />
                      </View>
                    );
                  }}
                />
              </View>
              <View style={styles.border1} />
              <View style={styles.savedAddView}>
                <View style={styles.svdTextView}>
                  <Text style={styles.svdText}>RECENT SEARCHES</Text>
                </View>
                <FlatList
                  data={RECENT_SEARCHES2}
                  keyExtractor={(item, index) => `${index}`}
                  renderItem={({item, index}) => {
                    return (
                      <View>
                        <View style={styles.addressView}>
                          <MaterialIcons
                            name="history"
                            size={40}
                            style={styles.workIcon}
                          />
                          <TouchableOpacity style={styles.currLoctext}>
                            <Text style={styles.saveAddtext1}>
                              {item.title}
                            </Text>
                            <Text style={styles.saveAddtext2}>
                              {item.address}
                            </Text>
                          </TouchableOpacity>
                        </View>
                        <View style={styles.border2} />
                      </View>
                    );
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default AddressScreen;

import {StyleSheet} from 'react-native';
import {
  HEIGHT,
  COLORS,
  WIDTH,
  FONTS,
  FONT_MEASURMENT,
} from '../../helper/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
    backgroundColor: COLORS.APP_PRIMARY,
  },
  mainHeaderText: {
    marginVertical: 40,
  },
  headertext1: {
    color: '#00000090',
    fontSize: FONT_MEASURMENT.medium,
  },
  headertext2: {
    color: COLORS.BLACK,
    fontSize: FONT_MEASURMENT.verylarge,
    fontFamily: FONTS.BOLD,
  },
  inputtext: {
    marginVertical: 10,
  },
  inputStyle: {
    borderBottomWidth: 1,
    borderColor: '#00000030',
    width: 360,
    height: 40,
    marginVertical: 10,
    flexDirection: 'row',
    paddingLeft: 3,
  },
  passinputContainer: {
    borderBottomWidth: 1,
    borderColor: '#00000030',
    width: 360,
    height: 40,
    marginVertical: 10,
    flexDirection: 'row',
    paddingLeft: 3,
    justifyContent: 'space-between',
  },
  textInput: {
    // width: Dimensions.get('window').width - 25,
    fontSize: FONT_MEASURMENT.medium,
  },
  eyeIcon: {
    color: '#00000070',
  },
  forgetPassText: {
    color: COLORS.ORANGE,
    textAlign: 'right',
    fontSize: FONT_MEASURMENT.title,
    marginVertical: 8,
  },
  horizontalrow: {
    borderWidth: 0.9,
    marginVertical: 5,
  },
  socialLinkContainer: {
    alignItems: 'center',
  },
  socialLinkText: {
    textAlign: 'center',
    marginVertical: 15,
  },
  icon: {
    color: COLORS.APP_PRIMARY,
    marginHorizontal: 5,
  },
  accountContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 50,
  },
  accountText: {
    fontSize: FONT_MEASURMENT.medium,
  },
  createButton: {
    borderBottomWidth: 1,
  },
  createText: {
    fontSize: FONT_MEASURMENT.medium,
    fontWeight: '700',
    marginBottom: 8,
    marginHorizontal: 8,
  },
  iconSocial: {
    color: COLORS.APP_PRIMARY,
    marginHorizontal: 20,
  },
});

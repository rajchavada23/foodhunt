import React from 'react';
import {View, TextInput, TouchableOpacity, ScrollView} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import * as Animatable from 'react-native-animatable';

// Relative path
import {styles} from './LoginStyles';
import Text from '../../components/Text';
import {STOCK_ANIMATION, COLORS} from '../../helper/Constants';
import LoginButton from '../../components/loginButton/LoginButton.js';
import SocialButton from '../../components/socialButton/SocialButton.js';

// Icon path
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

const fbIcon = (
  <MaterialCommunityIcons name="facebook" style={styles.iconSocial} size={30} />
);

const googleIcon = (
  <EvilIcons name="sc-google-plus" style={styles.iconSocial} size={30} />
);

const LoginScreen = () => {
  const navigation = useNavigation();
  return (
    <ScrollView
      contentContainerStyle={{flexGrow: 1}}
      showsVerticalScrollIndicator={false}>
      <Animatable.View
        animation={STOCK_ANIMATION}
        style={[styles.container, {backgroundColor: COLORS.APP_PRIMARY}]}>
        <View style={styles.mainHeaderText}>
          <Text style={styles.headertext1}>Welcome to</Text>
          <Text style={styles.headertext2}>STAR DELIVERY</Text>
        </View>
        <View style={styles.inputcontainer}>
          <Text style={styles.inputtext}>LOGIN TO YOUR ACCOUNT</Text>
          <View style={styles.inputStyle}>
            <TextInput
              style={styles.textInput}
              placeholder="Enter Email ID"
              keyboardType="default"
            />
          </View>
          <View style={styles.passinputContainer}>
            <TextInput
              style={styles.textInput}
              placeholder="Enter Password"
              keyboardType="default"
              secureTextEntry={true}
            />
            <MaterialIcons
              name="remove-red-eye"
              size={30}
              style={styles.eyeIcon}
            />
          </View>
          <TouchableOpacity>
            <Text style={styles.forgetPassText}>Forget Password?</Text>
          </TouchableOpacity>
        </View>
        <LoginButton title="Login" />
        <View style={styles.horizontalrow} />
        <View style={styles.socialLinkContainer}>
          <Text style={styles.socialLinkText}>
            CONTINUE WITH SOCIAL ACCOUNTS
          </Text>
          <SocialButton
            color="#5671EA"
            title="Continue with Facebook"
            icon={fbIcon}
          />
          <SocialButton
            color="#DC373B"
            title="Continue with Google"
            icon={googleIcon}
          />
        </View>
        <View style={styles.accountContainer}>
          <Text style={styles.accountText}>Dont have an account?</Text>
          <TouchableOpacity
            style={styles.createButton}
            onPress={() => {
              navigation.navigate('signup');
            }}>
            <Text style={styles.createText}>CREATE NOW</Text>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </ScrollView>
  );
};

export default LoginScreen;

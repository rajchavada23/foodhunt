import {StyleSheet} from 'react-native';
import {
  HEIGHT,
  COLORS,
  WIDTH,
  FONTS,
  FONT_MEASURMENT,
} from '../../helper/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  arrowIcon: {
    position: 'absolute',
    left: 15,
    top: 10,
    zIndex: 999,
  },
  mapView: {
    position: 'relative',
    height: HEIGHT / 3.5,
  },
  containerBody: {
    backgroundColor: COLORS.APP_PRIMARY,
    flex: 1,
  },
  containerLocation: {
    width: WIDTH - 40,
    alignSelf: 'center',
  },
  setDeliveryText: {
    fontFamily: FONTS.EXTRA_BOLD,
    fontSize: FONT_MEASURMENT.title,
    paddingVertical: 10,
  },
  titleText: {
    fontFamily: FONTS.BOLD,
    paddingTop: 10,
  },
  textLocation: {
    color: COLORS.WORKICON_COLOR,
    paddingLeft: 6,
    paddingVertical: 5,
  },
  horizontalLine: {
    borderWidth: 0.5,
    borderColor: COLORS.WORKICON_COLOR,
    marginBottom: 10,
  },
  targetIcon: {
    color: COLORS.BLUE,
    alignSelf: 'flex-end',
  },
  flatContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  saveasText: {
    fontFamily: FONTS.BOLD,
    paddingTop: 15,
  },
  verifyButton: {
    backgroundColor: COLORS.GREEN,
    alignItems: 'center',
    paddingVertical: 15,
  },
  buttontext: {
    color: COLORS.APP_PRIMARY,
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.BOLD,
  },
});

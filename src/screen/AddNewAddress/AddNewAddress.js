/* eslint-disable react/jsx-no-undef */
import React from 'react';
import {View, TouchableOpacity, FlatList} from 'react-native';
import MapView from 'react-native-maps';

// Navigation
import {useNavigation} from '@react-navigation/native';

// Helper
import {ADD_LOCATION} from '../../helper/jsonData';

// Relative path
import {styles} from './AddNewAddressStyle';
import Text from '../../components/Text';
import {COLORS} from '../../helper/Constants';

// Icon path
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const AddNewAddress = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <MapView
        style={styles.mapView}
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      />
      <TouchableOpacity
        style={styles.arrowIcon}
        onPress={() => {
          navigation.goBack();
        }}>
        <AntDesign name="arrowleft" size={30} />
      </TouchableOpacity>
      <View style={styles.containerBody}>
        <View style={styles.containerLocation}>
          <Text style={styles.setDeliveryText}>Set delivery Location</Text>
          <FlatList
            data={ADD_LOCATION}
            keyExtractor={(item, index) => `${index}`}
            renderItem={({item, index}) => {
              return (
                <View>
                  <View style={styles.flatContainer}>
                    <View>
                      <Text style={styles.titleText}>{item.title}</Text>
                      <TouchableOpacity>
                        <Text style={styles.textLocation}>{item.text}</Text>
                      </TouchableOpacity>
                    </View>
                    {item.type === 'target' ? (
                      <MaterialCommunityIcons
                        name="target"
                        size={40}
                        style={styles.targetIcon}
                      />
                    ) : null}
                  </View>
                  <View style={styles.horizontalLine} />
                </View>
              );
            }}
          />
          <Text style={styles.saveasText}>SAVE AS</Text>
        </View>
      </View>
      <TouchableOpacity style={styles.verifyButton}>
        <Text style={styles.buttontext}>SAVE LOCATION</Text>
      </TouchableOpacity>
    </View>
  );
};

export default AddNewAddress;

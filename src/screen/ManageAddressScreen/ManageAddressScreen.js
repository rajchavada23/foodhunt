/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react';
import {View, TouchableOpacity, FlatList, ScrollView} from 'react-native';

//Navigation
import {useNavigation} from '@react-navigation/native';

// Relative path
import {styles} from './ManageAddressStyle';
import Text from '../../components/Text';

//Icon Path
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {COLORS} from '../../helper/Constants';

// Helper
import {SAVED_ADDRESSES} from '../../helper/jsonData';

const manageAddressScreen = () => {
  const navigation = useNavigation();
  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <View style={styles.container}>
        <View style={styles.mainContainer}>
          <View style={styles.viewContainer}>
            <View style={styles.headerContainer}>
              <TouchableOpacity
                onPress={() => {
                  navigation.goBack();
                }}>
                <AntDesign
                  name="arrowleft"
                  size={30}
                  style={styles.arrowIcon}
                />
              </TouchableOpacity>
              <View style={styles.headerView}>
                <Text style={styles.headertext1}>SAVED ADDRESSES</Text>
                <Text style={styles.headertext2}>Manage Address</Text>
              </View>
            </View>
            <FlatList
              data={SAVED_ADDRESSES}
              keyExtractor={(item, index) => `${index}`}
              renderItem={({item, index}) => {
                return (
                  <View style={styles.bodyContainer}>
                    {item.type === 'office' ? (
                      <MaterialIcons
                        name="work"
                        size={40}
                        color={COLORS.WORKICON_COLOR}
                      />
                    ) : (
                      <Feather
                        name="map-pin"
                        size={40}
                        color={COLORS.WORKICON_COLOR}
                      />
                    )}
                    <View style={styles.textView}>
                      <Text style={styles.workText}>{item.title}</Text>
                      <Text style={styles.addressText}>{item.address}</Text>
                      <View style={styles.buttonView}>
                        <TouchableOpacity>
                          <Text style={styles.editText}>Edit</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                          <Text style={styles.editText}>Delete</Text>
                        </TouchableOpacity>
                      </View>
                      {item.empty === styles.itemInvisible ? (
                        <View style={styles.border1} />
                      ) : (
                        <View style={styles.itemInvisible} />
                      )}
                    </View>
                  </View>
                );
              }}
            />
            <TouchableOpacity
              style={styles.buttonContainer}
              onPress={() => {
                navigation.navigate('AddNewAddress');
              }}>
              <Text style={styles.buttonText}>Add New Address</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default manageAddressScreen;

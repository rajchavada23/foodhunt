import {StyleSheet} from 'react-native';
import {
  HEIGHT,
  COLORS,
  WIDTH,
  FONTS,
  FONT_MEASURMENT,
} from '../../helper/Constants';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.APP_PRIMARY,
  },
  mainContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewContainer: {
    width: WIDTH - 40,
  },
  headerContainer: {
    paddingTop: 25,
    paddingBottom: 15,
  },
  headerView: {
    marginVertical: 25,
  },
  headertext1: {
    fontSize: 22,
    fontWeight: '700',
  },
  headertext2: {
    fontSize: FONT_MEASURMENT.medium,
    color: COLORS.WORKICON_COLOR,
  },
  bodyContainer: {
    flexDirection: 'row',
  },
  textView: {
    flex: 1,
    paddingVertical: 1,
    paddingLeft: 25,
  },
  workText: {
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.BOLD,
  },
  addressText: {
    fontSize: FONT_MEASURMENT.small,
    color: COLORS.WORKICON_COLOR,
    fontFamily: FONTS.REGULAR,
    paddingVertical: 11,
  },
  buttonView: {
    flexDirection: 'row',
  },
  editText: {
    color: COLORS.ORANGE,
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.EXTRA_BOLD,
    paddingRight: 35,
  },
  border1: {
    borderWidth: 1,
    borderColor: '#E4E5E8',
    marginVertical: 25,
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  buttonContainer: {
    backgroundColor: COLORS.GREEN,
    paddingVertical: 15,
    marginLeft: 65,
    marginTop: 50,
    alignItems: 'center',
  },
  buttonText: {
    color: COLORS.APP_PRIMARY,
    fontSize: FONT_MEASURMENT.large,
    fontFamily: FONTS.EXTRA_BOLD,
  },
});
